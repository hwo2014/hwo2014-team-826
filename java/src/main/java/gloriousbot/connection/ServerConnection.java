package gloriousbot.connection;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.ReceiveMessageDeserializer;
import gloriousbot.messages.SendMessage;
import gloriousbot.messages.SendMessageSerializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ServerConnection implements AutoCloseable {
	
	private static final Gson gson = new GsonBuilder()
					.registerTypeAdapter(ReceiveMessage.class, new ReceiveMessageDeserializer())
					// Y U NO WORK??????
					.registerTypeAdapter(SendMessage.class, new SendMessageSerializer())
					.create();
	
	private BufferedReader reader;
	private PrintWriter writer;
	
	private Socket socket;

	private ServerConnection(BufferedReader reader, PrintWriter writer) {
		this.reader = reader;
		this.writer = writer;
	}
	
	private ServerConnection(String host, int port) throws IOException {
		this.socket = new Socket(host, port);
		this.writer = new PrintWriter(new OutputStreamWriter(this.socket.getOutputStream(), "utf-8"));
		this.reader = new BufferedReader(new InputStreamReader(this.socket.getInputStream(), "utf-8"));
	}
	
	public static void connect(String host, int port, Consumer<ServerConnection> consumer) {
		try (ServerConnection con = new ServerConnection(host, port)) {
			consumer.accept(con);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void send(SendMessage message) {
		String json = gson.toJson(new MessageWrapper(message));
		//System.out.println("> Send: " + json);
		this.writer.println(json);
		this.writer.flush();
	}
	
	// This blocks
	public ReceiveMessage readMessage() {
		try {
			String line = this.reader.readLine();
			if (line == null) {
				return null;
			}
			
			//System.out.println("< Recv: " + line);
			
			return gson.fromJson(line, ReceiveMessage.class);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void close() {
		if (this.writer != null) {
			this.writer.close();
		}
		if (this.reader != null) {
			try {
				this.reader.close();
			}
	
			catch (IOException e) {
				// ignore
			}
		}
		if (this.socket != null) {
			try {
				this.socket.close();
			}
			catch (IOException e) {
				// ignore
			}
		}
	}
}

// FULHACK
class MessageWrapper {
	public String msgType;
	public Object data;
	public Integer gameTick;
	
	public MessageWrapper(SendMessage sendMessage) {
		this.msgType = sendMessage.msgType();
		this.data = sendMessage.data();
		this.gameTick = sendMessage.gameTick;
	}
}
