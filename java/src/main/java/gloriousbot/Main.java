package gloriousbot;

import gloriousbot.connection.ServerConnection;
import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.SendMessage;
import gloriousbot.messages.receive.CarPositions;
import gloriousbot.messages.receive.Dnf;
import gloriousbot.messages.send.*;

public class Main {
    public static void main(String ... args) {
    	if (args.length < 4) {
    		System.err.println("Not enough arguments!");
    		return;
    	}
    	
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        
        ServerConnection.connect(host, port, conn -> {
        	conn.send(new JoinRace(botName, botKey, 1, "keimola", null));
        	
        	ReceiveMessage msg = null;
        	
        	while ((msg = conn.readMessage()) != null) {
        		SendMessage command = new Ping();

        		if (!(msg instanceof CarPositions)) {
        			System.out.println("Msg: " + msg.getClass().getSimpleName() + " (" + msg + ")");
        		} else {
        			CarPositions pos = ((CarPositions) msg);
        			double angle = 0.0;
        			if (pos.data.size() > 0) {
        				angle = pos.data.get(0).angle;
        			}

//        			if (Math.random() < 0.02) {
//        				String dir = Math.random() > 0.5 ? "Left" : "Right";
//        				command = new SwitchLane(dir);
//        			} else {
        				double speed = 0.0;
        				double angleAbs = Math.abs(angle);
        				if(angleAbs < 20) speed = 0.70;
        				else speed = Math.max(0.0, 0.70 - (angleAbs - 20)*0.025);
        				command = new Throttle(speed, msg);
        				System.out.println(""+angle +" "+ speed);
//        			}
        		}

        		conn.send(command);
        	}
        });
        
        System.out.println("Exiting program..");
    }

}

