package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.receive.objects.CarId;

public class Crash extends ReceiveMessage {
	
	public CarId data;
	
	public Crash() {
		super("crash");
	}
	
	@Override
	public String toString() {
		return data + " crashed!";
	}
	
}
