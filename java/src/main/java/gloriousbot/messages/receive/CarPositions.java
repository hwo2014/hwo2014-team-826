package gloriousbot.messages.receive;

import java.util.List;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.receive.objects.CarPosition;

public class CarPositions extends ReceiveMessage {
	
	public List<CarPosition> data;

	public CarPositions() {
		super("carPositions");
	}
	
}
