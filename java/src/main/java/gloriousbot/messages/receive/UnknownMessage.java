package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;

public class UnknownMessage extends ReceiveMessage {
	
	public Object data;
	
	public UnknownMessage() {
		super("unknownMessage");
	}
}
