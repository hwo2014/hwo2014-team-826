package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.receive.objects.CarId;

public class Spawn extends ReceiveMessage {
	
	public CarId data;
	
	public Spawn() {
		super("spawn");
	}
	
	@Override
	public String toString() {
		return data + " spawned again.";
	}
}
