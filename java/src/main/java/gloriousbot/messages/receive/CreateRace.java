package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.send.objects.BotId;

class CreateRaceData {
	public BotId botId;
	public String trackName;
	public String password;
	public Integer carCount;
}

public class CreateRace extends ReceiveMessage {
	
	public CreateRaceData data;

	public CreateRace() {
		super("createRace");
	}
}
