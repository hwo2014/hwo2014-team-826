package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.receive.objects.CarId;

class DnfData {
	public CarId car;
	public String reason;
}

public class Dnf extends ReceiveMessage {
	
	public DnfData data;
	
	public Dnf() {
		super("dnf");
	}
	
	@Override
	public String toString() {
		return data.car + " DNF, reason: " + data.reason;
	}
}
