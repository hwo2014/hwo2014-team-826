package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.receive.objects.CarId;

public class Finish extends ReceiveMessage {
	
	public CarId data;
	
	public Finish() {
		super("finish");
	}
	
	@Override
	public String toString() {
		return data + " finished the race!";
	}
}
