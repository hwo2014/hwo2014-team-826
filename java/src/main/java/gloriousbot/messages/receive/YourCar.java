package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.receive.objects.CarId;

public class YourCar extends ReceiveMessage {
	
	public CarId data;

	public YourCar() {
		super("yourCar");
	}
	
	@Override
	public String toString() {
		return data.toString();
	}
}
