package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;

public class TournamentEnd extends ReceiveMessage {
	
	public TournamentEnd() {
		super("tournamentEnd");
	}
	
}
