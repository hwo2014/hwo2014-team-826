package gloriousbot.messages.receive;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.receive.objects.CarId;

class CarDimensions {
	public Double length;
	public Double width;
	public Double guideFlagPosition;
}

class RaceSession {
	public Integer laps;
	public Long maxLapTimeMs;
	public Boolean quickRace;
}

class Car {
	public CarId id;
	public CarDimensions dimensions;
}

class TrackPiece {
	public Double length;
	public Double radius;
	public Double angle;
	@SerializedName("switch")
	public Boolean switchable;
}

class TrackLane {
	public Double distanceFromCenter;
	public Integer index;
}

class TrackPosition {
	public Double x;
	public Double y;
}

class TrackStartingPoint {
	public TrackPosition position;
	public Double angle;
}

class Track {
	public String id;
	public String name;
	public List<TrackPiece> pieces;
	public List<TrackLane> lanes;
	public TrackStartingPoint startingPoint;
}

class Race {
	public Track track;
	public List<Car> cars;
	public RaceSession raceSession;
}

class GameInitData {
	public Race race;
}

public class GameInit extends ReceiveMessage {
	public GameInitData data;
	
	public GameInit() {
		super("gameInit");
	}
}
