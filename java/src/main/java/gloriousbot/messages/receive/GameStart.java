package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;

public class GameStart extends ReceiveMessage {

	public GameStart() {
		super("gameStart");
	}
}
