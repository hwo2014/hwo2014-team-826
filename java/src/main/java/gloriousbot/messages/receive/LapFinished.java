package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.receive.objects.CarId;
import gloriousbot.messages.receive.objects.LapTime;
import gloriousbot.messages.receive.objects.RaceTime;

class Ranking {
	public Integer overall;
	public Integer fastestLap;
}

class LapFinishedData {
	public CarId car;
	public LapTime lapTime;
	public RaceTime raceTime;
	public Ranking ranking;
	
}

public class LapFinished extends ReceiveMessage {

	public LapFinishedData data;
	
	public LapFinished() {
		super("lapFinished");
	}
	
	public String toString() {
		return data.car + " finished lap " + data.lapTime.lap + " in " + data.lapTime.millis + " ms. Pos: " + data.ranking.overall + "!";
	}
}
