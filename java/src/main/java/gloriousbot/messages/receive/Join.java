package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;


class JoinData {
	public String name;
	public String key;
}

public class Join extends ReceiveMessage {
	
	public JoinData data;

	public Join() {
		super("join");
	}
	
}
