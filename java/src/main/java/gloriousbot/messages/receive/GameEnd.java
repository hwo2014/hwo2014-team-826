package gloriousbot.messages.receive;

import java.util.List;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.receive.objects.CarId;
import gloriousbot.messages.receive.objects.LapTime;
import gloriousbot.messages.receive.objects.RaceTime;

class GameResult {
	public CarId car;
	public RaceTime result;
}

class BestLap {
	public CarId car;
	public LapTime result;
}

class GameEndData {
	public List<GameResult> results;
	public List<BestLap> bestLaps;
}

public class GameEnd extends ReceiveMessage {
	
	public GameEndData data;

	public GameEnd() {
		super("gameEnd");
	}
	
	
}
