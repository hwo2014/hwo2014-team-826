package gloriousbot.messages.receive;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.send.objects.BotId;

class JoinRaceData {
	public BotId botId;
	public String trackName;
	public String password;
	public Integer carCount;
}

public class JoinRace extends ReceiveMessage {
	
	public JoinRaceData data;

	public JoinRace() {
		super("joinRace");
	}
}
