package gloriousbot.messages.receive.objects;

class Lane {
	public Integer startLaneIndex;
	public Integer endLaneIndex;
}

class PiecePosition {
	public Integer pieceIndex;
	public Double inPieceDistance;
	public Lane lane;
	public Integer lap;
}

public class CarPosition {
	public CarId id;
	public Double angle;
	public PiecePosition piecePosition;
}