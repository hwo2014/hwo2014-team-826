package gloriousbot.messages.receive.objects;

public class CarId {
	public String name;
	public String color;
	
	@Override
	public String toString()
	{
		return "Car [name=" + name + ", color=" + color + "]";
	}
}
