package gloriousbot.messages;

public class ReceiveMessage {
	public String msgType;
	public String gameId;
	public Integer gameTick;
	
	public ReceiveMessage(String msgType) {
		this.msgType = msgType;
	}
	
}
