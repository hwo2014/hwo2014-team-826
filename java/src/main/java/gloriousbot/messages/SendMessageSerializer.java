package gloriousbot.messages;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

class MessageWrapper {
	public String msgType;
	public Object data;
	public Integer gameTick;
	
	public MessageWrapper(SendMessage sendMessage) {
		this.msgType = sendMessage.msgType();
		this.data = sendMessage.data();
		this.gameTick = sendMessage.gameTick;
	}
}

public class SendMessageSerializer implements JsonSerializer<SendMessage> {
	
	@Override
	public JsonElement serialize(SendMessage src, Type typeOfSrc, JsonSerializationContext context) {
		MessageWrapper wrapper = new MessageWrapper(src);
		return context.serialize(wrapper);
	}
}
