package gloriousbot.messages.send;

import gloriousbot.messages.SendMessage;

public class Join extends SendMessage {
	public String name;
	public String key;
	
	public Join(String name, String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	public String msgType() {
		return "join";
	}
}
