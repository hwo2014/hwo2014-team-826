package gloriousbot.messages.send.objects;

public class BotId {
	public String name;
	public String key;
	
	public BotId(String name, String key) {
		this.name = name;
		this.key = key;
	}
}
