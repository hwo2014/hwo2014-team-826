package gloriousbot.messages.send;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.SendMessage;

public class Throttle extends SendMessage {
	public double value;
	
	public Throttle(double value) {
		this.value = value;
	}
	
	public Throttle(double value, ReceiveMessage respondTo) {
		super(respondTo);
		this.value = value;
	}

	@Override
	public String msgType() {
		return "throttle";
	}

	/**
	 * Overridden to send
	 * { "data": 0.0 }
	 * 
	 * instead of
	 * 
	 * { "data": { "value": 0.0 } }
	 */
	@Override
	public Object data() {
		return this.value;
	}
}
