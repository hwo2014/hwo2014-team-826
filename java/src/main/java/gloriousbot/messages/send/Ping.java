package gloriousbot.messages.send;

import gloriousbot.messages.SendMessage;

public class Ping extends SendMessage {
	
	@Override
	public String msgType() {
		return "ping";
	}
}
