package gloriousbot.messages.send;

import gloriousbot.messages.ReceiveMessage;
import gloriousbot.messages.SendMessage;

public class SwitchLane extends SendMessage {
	public String direction;
	
	public SwitchLane(String direction) {
		this.direction = direction;
	}
	
	public SwitchLane(String direction, ReceiveMessage respondTo) {
		super(respondTo);
		this.direction = direction;
	}

	@Override
	public String msgType() {
		return "switchLane";
	}

	@Override
	public Object data() {
		return this.direction;
	}
}
