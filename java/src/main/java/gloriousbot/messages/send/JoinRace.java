package gloriousbot.messages.send;

import gloriousbot.messages.SendMessage;
import gloriousbot.messages.send.objects.BotId;

public class JoinRace extends SendMessage {
	
	public BotId botId;
	public String trackName;  // optional
	public String password;   // optional
	public Integer carCount;
	
	public JoinRace(String name, String key, Integer carCount, String trackName, String password) {
		this.botId = new BotId(name, key);
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	@Override
	public String msgType() {
		return "joinRace";
	}
}
