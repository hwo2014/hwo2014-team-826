package gloriousbot.messages;

import gloriousbot.messages.receive.*;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class ReceiveMessageDeserializer implements JsonDeserializer<ReceiveMessage> {
	
	private static final Map<String, Class<? extends ReceiveMessage>> clazzes = new HashMap<>();
	static {
		// Init
		clazzes.put("join", Join.class);
		clazzes.put("joinRace", JoinRace.class);
		clazzes.put("createRace", CreateRace.class);
		clazzes.put("yourCar", YourCar.class);
		
		// Tournament start
		clazzes.put("gameInit", GameInit.class);
		
		// Game start
		clazzes.put("gameStart", GameStart.class);
		
		// Racing
		clazzes.put("carPositions", CarPositions.class);
		clazzes.put("dnf", Dnf.class);
		clazzes.put("crash", Crash.class);
		clazzes.put("spawn", Spawn.class);
		clazzes.put("lapFinished", LapFinished.class);
		clazzes.put("finish", Finish.class);
		
		// Game end
		clazzes.put("gameEnd", GameEnd.class);
		
		// Tournament end
		clazzes.put("tournamentEnd", TournamentEnd.class);
	}
	
	@Override
	public ReceiveMessage deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		
		String msgType = json.getAsJsonObject().get("msgType").getAsString();
		
		Class<? extends ReceiveMessage> clazz = clazzes.get(msgType);
		if (clazz == null) {
			clazz = UnknownMessage.class;
			System.err.println("Warning, received unknown message type: " + msgType);
			System.err.println("JSON: " + json);
		}
		
		return context.deserialize(json, clazz);
	}
}
