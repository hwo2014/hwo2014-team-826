package gloriousbot.messages;

/**
 * This represents the JSON message being sent.
 * 
 * Messages always have the form: 
 * 
 * {
 *   "msgType": "string",
 *   "data": object
 * }
 * 
 * Data can be a native object, i.e. (0.0) or an object
 * with nested properties.
 *
 */
public abstract class SendMessage {
	
	/**
	 * By default this is the object itself,
	 * Override to set a native value
	 */
	public abstract String msgType();
	public Object data() {
		return this;
	}
	public Integer gameTick;
    
    public SendMessage() {
    	// empty
    }
    
    /**
     * This copys the gameTick from the received message, in order to stay synchronized.
     */
    public SendMessage(ReceiveMessage respondTo) {
    	this.gameTick = respondTo.gameTick;
    }
    
}
